package fr.imta.mde.exemple;

public class RunExemple {

    public static void main(String[] args) {
        // Les classes générées par EMF utilisent toutes le patron Factory
        // il faut donc passer par une classe singleton pour instancier les
        // objets
        ExempleFactory factory = ExempleFactory.eINSTANCE;

        // il existe une méthode createXXX par classe concrète du modèle
        UneDeuxiemeClasse exemple = factory.createUneDeuxiemeClasse();

        // il y a un coupe getter/setter pour chaque propriété
        exemple.setValeur(456);
        exemple.setNom("foobar");

        // pour les relations 1 - 1 ça fonctionne comme les propriétés
        exemple.setAutre(exemple);

        // pour les relations X - * EMF génère uniquement un getter pour la collection
        exemple.getAutres().add(exemple);

        System.out.println(exemple);
        System.out.println(exemple.getMoyenne());
    }
}
